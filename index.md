---
layout: default
title: mouseparty
description: 
sitemap:
  priority: 0.9
  freq: 'weekly'
---

# mouseparty

mouseparty provides collection and enrichment infrastructure for lightweight security sensors. This infrastructure is made available to select ethical security researchers.

### Infrastructure

Currently, it ingests information sent to the following endpoints:

* **package.mouseparty.org** - collects reporting package information and records the source IP for HTTPS requests sent to it

These endpoints are for security research programs to report information. These endpoints are unauthenticated and process any information sent to them, unless blocked by rate-limiting or other security protections. For information on data processing, please see the [privacy](/privacy/) statement.

### So What?

If you have seen requests to mouseparty infrastructure, one of your systems or a system in your organization *likely could have been compromised*. One of the researchers affiliated with mouseparty has prevented this and is reporting this metadata to track, analyze, and report security issues on a best-effort basis.

You *can* block this infrastructure, but we don't recommend it, as it won't stop the initial attack vector you or your organization is vulnerable to. Your time would be better spent mitigating the initial attack vector.

Still concerned? Please check our [tenets](/tenets/) or reach out to the [maintainer](https://chris.partridge.tech/about/) with questions.

### Researchers

* [tweedge](https://chris.partridge.tech/) maintains mouseparty & accidentally found a neat typosquatting prioritization method