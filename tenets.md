---
layout: default
title: tenets @ mouseparty
permalink: /tenets/
description: 
sitemap:
  priority: 0.9
  freq: 'weekly'
---

# tenets @ mouseparty

Research that mouseparty provides collection infrastructure for must abide by the following tenets. If you note anything that could be improved, reach out so we can assess. Our projects serve the broader security and development communities, and we value your feedback.

### Democratize Security

All research must attempt to inform the user of what security issue has been prevented, give basic guidance for remediation, and provide a link for follow-ups.

### No On-Host Data Collection

On-host data collection could pose limited privacy risks. For additional research options or voluntary data reporting, mouseparty will use voluntary web forms or CLI tools; sensitive data collection will never be autonomous.