---
layout: default
title: privacy @ mouseparty
permalink: /privacy/
description: 
sitemap:
  priority: 0.9
  freq: 'weekly'
---

# privacy @ mouseparty

Any requests made to mouseparty are considered untrusted. As these are unauthenticated endpoints, we have no way to certify exactly what is or isn't due to a compromise. However, we can still use this to look for trends or evaluate the efficacy of our projects in conjunction with other signals (such as download metrics from package indices).

### Data Collection

Currently, there is no data collected on systems by any mouseparty-utilizing project. A request is made outbound to mouseparty which states what package was downloaded - no on-system data is collected or reported.

### Data Processing

Incoming requests may be enriched via external API providers, such as querying a WHOIS information provider with an IP to get the ownership information.

### Data Reporting

Raw data will never be published publicly without prior consent, though aggregate information (such as the number of requests from certain IP ranges, or request totals by country) may be published as part of research or disclosures.

Data pertaining to specific private security disclosures may be sent in raw or non-aggregated form via secure methods.

### Data Retention

Raw data may be kept for up to thirteen (13) calendar months to analyze long-term trends.

### User Control

If you would like us to purge any particular data from our systems or prevent processing data from certain sources (such as an IP address range), please reach out to the [maintainer](https://chris.partridge.tech/about/) with reasonable proof that the data or source belongs to you or the company you represent.
